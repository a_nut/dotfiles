#!/usr/bin/zsh

state_f='""'
state_c='""'
state_n='""'

vis=false
state=state_n
val=100

if [[ -d /sys/class/power_supply/BAT0 ]]; then
    vis=true
    val=$(cat /sys/class/power_supply/BAT0/capacity)
    case $(cat /sys/class/power_supply/BAT0/status) in
        Charging)
            state=state_c
            ;;
        Full)
            state=state_f
            ;;
        *)
            state=state_n
            ;;
    esac
fi

echo '{
    "vis"='$vis',
    "val"='$val',
    "state"='$state'
}'
