# bc898 colour scheme
# by a_nut
# file based off gruvbox.kak
# to be used with bc898 as terminal colours

#face global Default   default,default

# Code highlighting
face global value     bright-black
face global type      green
face global variable  green
face global module    bright-yellow
face global function  bright-green
face global string    bright-white
face global keyword   bright-red
face global operator  bright-red
face global attribute magenta
face global comment   blue
face global meta      magenta
face global builtin   red+b

# Markdown highlighting
face global title     green+b
face global header    yellow
face global bold      bright-black+b
face global italic    default+i
face global mono      default,black
face global block     default,black
face global link      cyan+u
face global bullet    bright-red+b
face global list      bright-yellow

face global PrimarySelection   bright-cyan,white+fg
face global SecondarySelection cyan,black+fg
face global PrimaryCursor      red,bright-cyan+fg
face global SecondaryCursor    red,bright-blue+fg
face global PrimaryCursorEol   black,red+fg
face global SecondaryCursorEol black,blue+fg

face global LineNumbers        bright-blue,black
face global LineNumberCursor   bright-cyan,white+fg
face global LineNumbersWrapped bright-magenta,white

face global MenuForeground     bright-cyan,white
face global MenuBackground     default,black
face global MenuInfo           bright-green,black
face global Information        bright-black,black
face global Error              bright-yellow,red+b

face global StatusLine         default,black
face global StatusLineMode     cyan+b
face global StatusLineInfo     green
face global StatusLineValue    bright-yellow
face global StatusCursor       red,cyan
face global Prompt             bright-green

face global MatchingChar       red,magenta+fg
face global BufferPadding      blue,default
face global Whitespace         white+f

# kak-crosshairs
face global crosshairs_line    default,black+b
face global crosshairs_column  default,black
