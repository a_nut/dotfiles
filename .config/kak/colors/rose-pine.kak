declare-option str base              'default'
declare-option str base_col          'rgb:191724'
declare-option str surface           'rgb:1f1d2e'
declare-option str overlay           'rgb:26233a'
declare-option str inactive          'rgb:555169'
declare-option str subtle            'rgb:6e6a86'
declare-option str text              'rgb:e0def4'
declare-option str love              'rgb:eb6f92'
declare-option str gold              'rgb:f6c177'
declare-option str rose              'rgb:ebbcba'
declare-option str pine              'rgb:31748f'
declare-option str foam              'rgb:9ccfd8'
declare-option str iris              'rgb:c4a7e7'
declare-option str highlight         'rgb:2a2837'
declare-option str highlightInactive 'rgb:211f2d'
declare-option str highlightOverlay  'rgb:3a384a'

face global value      "%opt{iris}"
face global type       "%opt{iris}"
face global identifier "%opt{subtle}"
face global string     "%opt{gold}"
face global keyword    "%opt{pine}"
face global operator   "default"
face global attribute  "%opt{foam}"
face global comment    "%opt{subtle}+i"
face global meta       "%opt{foam}"
face global builtin    "%opt{pine}"

face global variable "%opt{rose}"
face global module   "%opt{foam}"
face global function "%opt{foam}"

face global title  "%opt{rose}+b"
face global header "%opt{rose}"
face global bold   "default"
face global italic "default"
face global mono   "%opt{rose}"
face global block  "%opt{pine}"
face global link   "%opt{love}"
face global bullet "%opt{rose}"
face global list   "%opt{rose}"

face global Default            "%opt{text},%opt{base}"
face global PrimarySelection   "default,%opt{highlight}"
face global SecondarySelection "default,%opt{highlightInactive}"
face global PrimaryCursor      "%opt{base},%opt{text}"
face global SecondaryCursor    "default,%opt{inactive}"
face global LineNumbers        "%opt{subtle},%opt{base}"
face global LineNumberCursor   "%opt{gold},%opt{base}"
face global MenuForeground     "%opt{text},%opt{overlay}"
face global MenuBackground     "%opt{subtle},%opt{surface}"
face global MenuInfo           "%opt{inactive}"
face global Information        "%opt{text},%opt{overlay}"
face global Error              "%opt{base_col},%opt{love}"
face global StatusLine         "%opt{text},%opt{surface}"
face global StatusLineMode     "%opt{rose}"
face global StatusLineInfo     "%opt{foam}"
face global StatusLineValue    "%opt{pine}"
face global StatusCursor       "%opt{base},%opt{text}"
face global Prompt             "%opt{foam},%opt{surface}"
face global MatchingChar       "default+u"
face global BufferPadding      "%opt{inactive},%opt{base}"
face global Whitespace         "%opt{inactive}+f"
