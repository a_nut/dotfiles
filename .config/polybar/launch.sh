#!/bin/sh
cd "$HOME/.config/polybar/" || exit 1

# env vars
export INTERFACE_WL=$(ip addr | awk '{print $2}' | grep wl | sed 's/://' | tail -n 1)
export INTERFACE_EN=$(ip addr | awk '{print $2}' | grep en | sed 's/://' | tail -n 1)

if pgrep polybar; then
	polybar-msg cmd restart
else
	for mon in $(polybar --list-monitors | cut -d: -f1); do
		if grep '\[bar/'"$mon"'\]' config; then
			polybar "$mon" &
		else
			MONITOR=$mon polybar main &
		fi
	done
fi
bspc rule -a polybar layer=above
