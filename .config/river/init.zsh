#!/usr/bin/env zsh

riverctl spawn 'rivertile -outer-padding 0 -view-padding 0'
riverctl default-layout rivertile

riverctl input pointer-1267-12615-DELL0B24:00_04F3:3147_Touchpad tap enabled
riverctl input pointer-1267-12615-DELL0B24:00_04F3:3147_Touchpad disable-while-typing disabled
riverctl xcursor-theme 'Breeze_Hacked'
riverctl attach-mode top

riverctl background-color 0x1E1E2E
riverctl border-width 4
#colored borders
riverctl border-color-focused 0xC9CBFF
riverctl border-color-unfocused 0x1E1E2E
riverctl border-color-urgent 0xE64553

pgrep swaync &>/dev/null && pkill swaync &>/dev/null
swaync &

source ~/.config/river/keymap.zsh

# demons
pgrep wbg &>/dev/null && pkill wbg &>/dev/null
wbg ~/.config/river/wal.* &>/dev/null &

pgrep waybar &>/dev/null && pkill waybar &>/dev/null
waybar &>/dev/null &

pgrep polkit-gnome || /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &>/dev/null &

config="${XDG_CONFIG_HOME:-$HOME/.config}/gtk-3.0/settings.ini"
if [ ! -f "$config" ]; then exit 1; fi

gnome_schema="org.gnome.desktop.interface"
gtk_theme="$(grep 'gtk-theme-name' "$config" | sed 's/.*\s*=\s*//')"
icon_theme="$(grep 'gtk-icon-theme-name' "$config" | sed 's/.*\s*=\s*//')"
cursor_theme="$(grep 'gtk-cursor-theme-name' "$config" | sed 's/.*\s*=\s*//')"
font_name="$(grep 'gtk-font-name' "$config" | sed 's/.*\s*=\s*//')"
gsettings set "$gnome_schema" gtk-theme "$gtk_theme"
gsettings set "$gnome_schema" icon-theme "$icon_theme"
gsettings set "$gnome_schema" cursor-theme "$cursor_theme"
gsettings set "$gnome_schema" font-name "$font_name"
