#!/usr/bin/env zsh

# movement
riverctl map normal Mod4 j focus-view previous
riverctl map normal Mod4 k focus-view next
riverctl map normal Mod4 a zoom
riverctl map normal Mod4 s toggle-float
riverctl map normal Mod4 f toggle-fullscreen

# mouse
riverctl map-pointer normal Mod4 BTN_LEFT move-view
riverctl map-pointer normal Mod4 BTN_RIGHT resize-view
riverctl map-pointer normal Mod4+Shift BTN_LEFT resize-view

# tags
for i in {1..8}; do
	riverctl map normal Mod4 $i set-focused-tags $((1 << (i-1)))
	riverctl map normal Mod4+Shift $i set-view-tags $((1 << (i-1)))
	riverctl map normal Mod4+Mod1 $i toggle-focused-tags $((1 << (i-1)))
	riverctl map normal Mod4+Control $i toggle-view-tags $((1 << (i-1)))
done

# control
riverctl map normal Mod4 q close
riverctl map normal Mod4+Mod1 q exit
riverctl map normal Mod4 r spawn 'zsh ~/.config/river/init'
riverctl map normal Mod4 9 spawn waylock
riverctl map normal Mod4 0 spawn 'wlogout -p layer-shell -c 12 -r 12 -l ~/.config/wlogout/layout -C ~/.config/wlogout/style.css'

# screenshots
swapsave='swappy -f - -o ~/scrot/"$(date '\''+scrot_%y-%m-%d_%H:%M:%S.png'\'')" <<<$(wl-paste)'
riverctl map normal Mod4 p spawn 'grimshot copy output'
riverctl map normal Mod4+Shift p spawn 'grimshot copy active'
riverctl map normal Mod4+Control p spawn 'grimshot copy area'
riverctl map normal Mod4+Control+Shift p spawn "$swapsave"

# apps
riverctl map normal Mod4 Space spawn 'fuzzel -b 1e1e2eff -t d9e0eeff -m fae3b0ff -s 575268ff -S d9e0eeff -B 0 -r 0'
riverctl map normal Mod4 Return spawn 'wezterm-gui'
riverctl map normal Mod4 t spawn 'wezterm-gui'
riverctl map normal Mod4 Tab spawn 'swaync-client -t'

# media
riverctl map normal None XF86AudioPlay spawn 'mpc toggle'
riverctl map normal None XF86AudioMute spawn 'ponymix toggle'
riverctl map normal None XF86AudioRaiseVolume spawn 'ponymix increase 5'
riverctl map normal None XF86AudioLowerVolume spawn 'ponymix decrease 5'

# backlight
riverctl map normal None XF86_MonBrightnessUp spawn 'light -A 10'
riverctl map normal None XF86_MonBrightnessDown spawn 'light -U 10'
