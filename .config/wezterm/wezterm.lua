local wezterm = require('wezterm');

local colors = require('colors/rose-pine').colors()
local window_frame = require('colors/rose-pine').window_frame()

return {
	--colors = {
	--	foreground      = "#D9E0EE",
	--	background      = "#1E1E2E",
	--	selection_fg    = "#D9E0EE",
	--	selection_bg    = "#575268",
	--	cursor_bg       = "#F5E0DC",
	--	cursor_fg       = "#1E1E2E",
	--	cursor_border   = "#1E1E2E",
	--	splits          = "#575268",
	--	scrollbar_thumb = "#575268",
	--	visual_bell     = "#302D41",
	--	ansi            = {"#6E6C7E", "#F28FAD", "#ABE9B3", "#FAE3B0", "#96CDFB", "#F5C2E7", "#89DCEB", "#D9E0EE"},
	--	brights         = {"#988BA2", "#F28FAD", "#ABE9B3", "#FAE3B0", "#96CDFB", "#F5C2E7", "#89DCEB", "#D9E0EE"},

	--	tab_bar = {
	--		background = "#161320",
	--		active_tab = {
	--			bg_color = "#575268",
	--			fg_color = "#F5C2E7",
	--		},
	--		inactive_tab = {
	--			bg_color = "#1E1E2E",
	--			fg_color = "#D9E0EE",
	--		},
	--		inactive_tab_hover = {
	--			bg_color = "#575268",
	--			fg_color = "#D9E0EE",
	--		},
	--		new_tab = {
	--			bg_color = "#15121C",
	--			fg_color = "#6E6C7C",
	--		},
	--		new_tab_hover = {
	--			bg_color = "#575268",
	--			fg_color = "#D9E0EE",
	--			italic = true,
	--		},
	--	},
	--},
	colors = colors,
	window_frame = window_frame,
	window_background_opacity = 0.78,
	text_background_opacity = 0.4,
	
	font = wezterm.font("Iosevka Term"),
	font_size = 12,
	hide_tab_bar_if_only_one_tab = true,
	use_fancy_tab_bar = false,
	enable_wayland = true,
}
