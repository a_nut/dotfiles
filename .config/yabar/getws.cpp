#include <iostream>
#include <string>
#include <cstdlib>
#include <vector>
#include <optional>

//#include <i3/ipc.h>
#include <i3-ipc++/i3_ipc.hpp>
#include <i3-ipc++/i3_containers.hpp>

const int wscount = 8;
const std::string seperator = "<span foreground=\"#998f84\"> | </span>";
const std::string wsempty = "<span foreground=\"#242321\">―</span>";
const std::string wsinactive = "<span foreground=\"#f8f6f2\">●</span>";
const std::string wsactive = "<span foreground=\"#0a9dff\">●</span>";
const std::string wsurgent = "<span foreground=\"#ff2c4b\">!!</span>";

std::string wsparse(i3_containers::workspace ws) {
	if(ws.is_urgent) {
		return wsurgent;
	}else if(ws.is_visible) {
		return wsactive;
	}else {
		return wsinactive;
	}
}

int main() {
	i3_ipc i3;
	std::vector<i3_containers::workspace> workspaces = i3.get_workspaces();

	std::string wsarray[wscount];
	for(int i=0;i<wscount;i++) {
		wsarray[i] = wsempty;
	}

	for(int i=0;i<workspaces.size();i++) {
		int ind = workspaces[i].num.value_or(i) - 1;
		wsarray[ind] = wsparse(workspaces[i]);
	}

	std::cout << wsarray[0];
	for(int i=1;i<wscount;i++) {
		std::cout << seperator << wsarray[i];
	}
	std::cout << "\n";

	return 0;
}
