#!/usr/bin/dash
# ⚫■ - for copy-paste

# options

wscount=8
# using pango-markup in yabar
seperator='<span foreground="#998f84"> | </span>'
wsempty='<span foreground="#242321">--</span>'
wsinactive='<span foreground="#f8f6f2">⚫</span>'
wsactive='<span foreground="#0a9dff">⚫</span>'
wsurgent='<span foreground="#ff2c4b">!!</span>'

# with yabar escape
#seperator='!Yfg0xff998f84Y! | '
#wsempty='!Yfg0xff242321Y!-'
#wsinactive='!Yfg0xfff8f6f2Y!■'
#wsactive='!Yfg0xff0a9dffY!■'
#wsurgent='!Yfg0xffff2c4bY!■'

# get workspaces and parse them

wsjson=$(i3-msg -t get_workspaces)
wsopencount=$(echo $wsjson | jq -cM '. | length')

# initiate an array containing the state of the workspaces
wsarray='0'
for i in $(seq 0 $(($wscount - 2)))
do
	wsarray="$wsarray 0"
done

# returns the state of a workspace
parse_workspace()
{
	# Input json encoded workspace object from i3-msg
	# Empty workspaces should be handled outside of this function
	if [ "$(echo $1 | jq -cM '.urgent')" = 'true' ]
	then
		return 3
	fi
	if [ "$(echo $1 | jq -cM '.visible')" = 'true' ]
	then
		return 2
	fi
	return 1
}

# update wsarray with the correct states
for i in $(seq 0 $(($wsopencount - 1)))
do
	wsobj=$(echo $wsjson | jq -cM '.['"$i"']')
	wsnum=$(echo $wsobj | jq -cM '.num')
	parse_workspace $wsobj
	wsstate=$?
	wsarray=$(echo $wsarray | awk '$'"$wsnum"'="'$wsstate'"')
done

# Format output with pango
output=''
for i in $(seq 1 $(echo $wsarray | tr -d '\n' | wc -m))
do
	chara=$(echo $wsarray | cut -c $i)
	case $chara in
		'0')
			output="$output""$wsempty"
			;;
		'1')
			output="$output""$wsinactive"
			;;
		'2')
			output="$output""$wsactive"
			;;
		'3')
			output="$output""$wsurgent"
			;;
		' ')
			output="$output""$seperator"
			;;
	esac
done
#echo $wsarray
echo " $output "
