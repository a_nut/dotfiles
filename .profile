#!/bin/sh

# ~/.profile

# Run an init on first login per system-start
[ ! -f "/tmp/$USER-init" ] &&\
	touch "/tmp/$USER-init" &&\
	. "$HOME/.sv/init" 2>&1 | socat stdin unix-sendto:/dev/log

# SSH agent
SSH_ENV="$HOME/.ssh/environment"

start_agent() {
	echo "Initialising new SSH agent..."
	/usr/bin/ssh-agent | sed 's/^echo/#echo/' > "${SSH_ENV}"
	echo succeeded
	chmod 600 "${SSH_ENV}"
	. "${SSH_ENV}" > /dev/null
}

# Source SSH settings, if applicable

if [ -f "${SSH_ENV}" ]; then
	. "${SSH_ENV}" > /dev/null
	#ps ${SSH_AGENT_PID} doesn't work under cywgin
	ps -ef | grep ${SSH_AGENT_PID} | grep ssh-agent$ > /dev/null || {
		start_agent;
	}
else
	start_agent;
fi
. "$HOME/.cargo/env"
