"	Plug
if empty(glob('~/.vim/autoload/plug.vim'))
	silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
		\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin()
Plug 'vim-syntastic/syntastic'
Plug 'vim-airline/vim-airline'
Plug 'rust-lang/rust.vim'
Plug 'vim-airline/vim-airline-themes'
Plug 'jiangmiao/auto-pairs'
Plug 'ntpeters/vim-better-whitespace'
Plug 'ervandew/supertab'
Plug 'tpope/vim-surround'
call plug#end()
filetype indent off
"	Settings

" Colours/ui
"colorscheme badwolf
"let g:airline_theme='badwolf'
let g:airline_powerline_fonts=1
syntax on
set showmatch

set hlsearch
set incsearch
set ignorecase
set smartcase

set nu rnu
set showcmd
set cursorline
set wildmenu
set wildmode=longest:list,full
set mouse=a

" Indent/folding
filetype plugin on
set smartindent
set tabstop=4
set softtabstop=4
set shiftwidth=4
"set expandtab

set foldenable
set foldlevelstart=3
set foldmethod=indent

set omnifunc=syntaxcomplete#Complete

" syntastic
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

let g:syntastic_cpp_compiler = 'g++'
let g:syntastic_cpp_compiler_options = ' -std=c++17'
"	Simple commands
" Capitals
command WQ wq
command Wq wq
command W w
command Q q

" Binds
nnoremap <silent> <C-l> :noh<CR><C-L>
nnoremap <space> za
