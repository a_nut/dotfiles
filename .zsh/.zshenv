#!/usr/bin/env zsh

export PATH="/bin:/usr/local/sbin:/usr/bin:/usr/local/bin:$HOME/.local/share/cargo/bin:$HOME/.local/share/yarn/bin:$HOME/.local/bin"
export CARGO_HOME="$HOME/.local/share/cargo"
export EDITOR='kak'
export TERMINAL='footclient'
export BROWSER='firefox'
export PAGER='less'
export LESS='-R --mouse --wheel-lines=2'
export ZDOTDIR="$HOME/.zsh"
export XDG_CONFIG_HOME="$HOME/.config"
export QT_QPA_PLATFORMTHEME='gtk2'
[[ -f $HOME/.cargo/env ]] && . "$HOME/.cargo/env"
export XDG_RUNTIME_DIR="/run/user/$UID"
export XCURSOR_THEME=Breeze_Hacked
export GTK_THEME=rose-pine-gtk
export WLR_NO_HARDWARE_CURSORS=1
export LC_ALL=en_AU.UTF-8
export LANG=en_AU.UTF-8
export MOZ_ENABLE_WAYLAND=1
export BAT_PAGER=''
export GTK2_RC_FILES=~/.local/share/themes/rose-pine-gtk/gtk-2.0/gtkrc
export SSH_AUTH_SOCK="${XDG_RUNTIME_DIR}/ssh-agent.socket"

export XDG_CURRENT_DESKTOP=Sway
