# The following lines were added by compinstall

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _complete _ignored _approximate
zstyle ':completion:*' expand suffix
zstyle ':completion:*' file-sort access
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' ignore-parents pwd ..
zstyle ':completion:*' insert-unambiguous false
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'm:{[:lower:]}={[:upper:]}' 'l:|=* r:|=*'
zstyle ':completion:*' max-errors 1
zstyle ':completion:*' menu select=1
zstyle ':completion:*' original true
zstyle ':completion:*' preserve-prefix '//[^/]##/'
zstyle ':completion:*' prompt '%e corrections were suggested'
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' verbose true
zstyle :compinstall filename '/home/death4lyfe/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE="/tmp/$USER-zshHist"
HISTSIZE=1000
SAVEHIST=3000
setopt appendhistory autocd extendedglob notify
unsetopt beep
bindkey -v
# End of lines configured by zsh-newuser-install

# Custom Lines

# Pre
cl() {
	printf '\n%.0s' $(seq 1 200)
	zle && zle reset-prompt
	true
}

function osc7-pwd() {
	emulate -L zsh # also sets localoptions for us
	setopt extendedglob
	local LC_ALL=C
	printf '\e]7;file://%s%s\e\' $HOST ${PWD//(#m)([^@-Za-z&-;_~])/%${(l:2::0:)$(([##16]#MATCH))}}
}

function chpwd-osc7-pwd() {
	(( ZSH_SUBSHELL )) || osc7-pwd
}
chpwd_functions+=(chpwd-osc7-pwd)
function osc-precmd() {
	print -Pn "\e]133;A\e\\"
	if ! builtin zle; then
		print -n "\e]133;D\e\\"
	fi
}
precmd_functions+=(osc-precmd)
function osc-preexec() {
	print -n "\e]133;C\e\\"
}
preexec_functions+=(osc-preexec)

bindkey "^[[3~" delete-char

autoload -U colors && colors
autoload -U edit-command-line
zle -N edit-command-line
bindkey -M vicmd v edit-command-line
bindkey "^x^e" edit-command-line

source $ZDOTDIR/.sh_aliases
eval $PRERUN
if [[ -f $ZDOTDIR/.zshrc.local ]]; then
	source $ZDOTDIR/.zshrc.local
fi

# inter
#source ~/.ssh/environment
cl

zgenom_plugins() {
	zgenom load hlissner/zsh-autopair
	zgenom load zdharma-continuum/fast-syntax-highlighting
	zgenom load jeffreytse/zsh-vi-mode
	zgenom load trystan2k/zsh-tab-title
	zgenom load joshskidmore/zsh-fzf-history-search
}

install_zgenom() {
	rm -rf $ZGEN_DIR
	git clone https://github.com/jandamm/zgenom $ZGEN_DIR
	unfunction zgenom
	source $ZGEN_DIR/zgenom.zsh
	zgenom_plugins
	zgenom save
}

export ZGEN_DIR="$ZDOTDIR/zgenom"
if [[ ! -f $ZGEN_DIR/zgenom.zsh ]]; then
	local do_install_zgenom="n"
	read -q do_install_zgenom"?zgenom not found, install it? (y/n): "
	echo
	if [[ $do_install_zgenom == "y" ]]; then
		install_zgenom
	else
		mkdir -p $ZGEN_DIR
		echo "zgenom() {}" >$ZGEN_DIR/zgenom.zsh
		echo 'Empty zgenom.zsh created, invoke `install_zgenom` if you change your mind'
	fi
fi
source $ZGEN_DIR/zgenom.zsh

eval $RUN
if ! zgenom saved; then
	zgenom_plugins
	zgenom save
fi

source ~/.zsh/tqprompt.zsh

bindkey '^?' backward-delete-char
bindkey '^W' backward-kill-word
bindkey '^h' backward-delete-char
bindkey '^U' backward-kill-line

zle -N clear-screen cl

ZSH_TAB_TITLE_ENABLE_FULL_COMMAND=true
ZSH_TAB_TITLE_ADDITIONAL_TERMS='kitty|foot'
#source /home/jj/.config/broot/launcher/bash/br

#eval $(thefuck --alias)

# Post
eval $POSTRUN
unset PRERUN RUN POSTRUN

# Other
COLORTEST='
                 40m     41m     42m     43m     44m     45m     46m     47m
     m [m  gYw   [m[40m  gYw  [0m [m[41m  gYw  [0m [m[42m  gYw  [0m [m[43m  gYw  [0m [m[44m  gYw  [0m [m[45m  gYw  [0m [m[46m  gYw  [0m [m[47m  gYw  [0m
    1m [1m  gYw   [1m[40m  gYw  [0m [1m[41m  gYw  [0m [1m[42m  gYw  [0m [1m[43m  gYw  [0m [1m[44m  gYw  [0m [1m[45m  gYw  [0m [1m[46m  gYw  [0m [1m[47m  gYw  [0m
   30m [30m  gYw   [30m[40m  gYw  [0m [30m[41m  gYw  [0m [30m[42m  gYw  [0m [30m[43m  gYw  [0m [30m[44m  gYw  [0m [30m[45m  gYw  [0m [30m[46m  gYw  [0m [30m[47m  gYw  [0m
 1;30m [1;30m  gYw   [1;30m[40m  gYw  [0m [1;30m[41m  gYw  [0m [1;30m[42m  gYw  [0m [1;30m[43m  gYw  [0m [1;30m[44m  gYw  [0m [1;30m[45m  gYw  [0m [1;30m[46m  gYw  [0m [1;30m[47m  gYw  [0m
   31m [31m  gYw   [31m[40m  gYw  [0m [31m[41m  gYw  [0m [31m[42m  gYw  [0m [31m[43m  gYw  [0m [31m[44m  gYw  [0m [31m[45m  gYw  [0m [31m[46m  gYw  [0m [31m[47m  gYw  [0m
 1;31m [1;31m  gYw   [1;31m[40m  gYw  [0m [1;31m[41m  gYw  [0m [1;31m[42m  gYw  [0m [1;31m[43m  gYw  [0m [1;31m[44m  gYw  [0m [1;31m[45m  gYw  [0m [1;31m[46m  gYw  [0m [1;31m[47m  gYw  [0m
   32m [32m  gYw   [32m[40m  gYw  [0m [32m[41m  gYw  [0m [32m[42m  gYw  [0m [32m[43m  gYw  [0m [32m[44m  gYw  [0m [32m[45m  gYw  [0m [32m[46m  gYw  [0m [32m[47m  gYw  [0m
 1;32m [1;32m  gYw   [1;32m[40m  gYw  [0m [1;32m[41m  gYw  [0m [1;32m[42m  gYw  [0m [1;32m[43m  gYw  [0m [1;32m[44m  gYw  [0m [1;32m[45m  gYw  [0m [1;32m[46m  gYw  [0m [1;32m[47m  gYw  [0m
   33m [33m  gYw   [33m[40m  gYw  [0m [33m[41m  gYw  [0m [33m[42m  gYw  [0m [33m[43m  gYw  [0m [33m[44m  gYw  [0m [33m[45m  gYw  [0m [33m[46m  gYw  [0m [33m[47m  gYw  [0m
 1;33m [1;33m  gYw   [1;33m[40m  gYw  [0m [1;33m[41m  gYw  [0m [1;33m[42m  gYw  [0m [1;33m[43m  gYw  [0m [1;33m[44m  gYw  [0m [1;33m[45m  gYw  [0m [1;33m[46m  gYw  [0m [1;33m[47m  gYw  [0m
   34m [34m  gYw   [34m[40m  gYw  [0m [34m[41m  gYw  [0m [34m[42m  gYw  [0m [34m[43m  gYw  [0m [34m[44m  gYw  [0m [34m[45m  gYw  [0m [34m[46m  gYw  [0m [34m[47m  gYw  [0m
 1;34m [1;34m  gYw   [1;34m[40m  gYw  [0m [1;34m[41m  gYw  [0m [1;34m[42m  gYw  [0m [1;34m[43m  gYw  [0m [1;34m[44m  gYw  [0m [1;34m[45m  gYw  [0m [1;34m[46m  gYw  [0m [1;34m[47m  gYw  [0m
   35m [35m  gYw   [35m[40m  gYw  [0m [35m[41m  gYw  [0m [35m[42m  gYw  [0m [35m[43m  gYw  [0m [35m[44m  gYw  [0m [35m[45m  gYw  [0m [35m[46m  gYw  [0m [35m[47m  gYw  [0m
 1;35m [1;35m  gYw   [1;35m[40m  gYw  [0m [1;35m[41m  gYw  [0m [1;35m[42m  gYw  [0m [1;35m[43m  gYw  [0m [1;35m[44m  gYw  [0m [1;35m[45m  gYw  [0m [1;35m[46m  gYw  [0m [1;35m[47m  gYw  [0m
   36m [36m  gYw   [36m[40m  gYw  [0m [36m[41m  gYw  [0m [36m[42m  gYw  [0m [36m[43m  gYw  [0m [36m[44m  gYw  [0m [36m[45m  gYw  [0m [36m[46m  gYw  [0m [36m[47m  gYw  [0m
 1;36m [1;36m  gYw   [1;36m[40m  gYw  [0m [1;36m[41m  gYw  [0m [1;36m[42m  gYw  [0m [1;36m[43m  gYw  [0m [1;36m[44m  gYw  [0m [1;36m[45m  gYw  [0m [1;36m[46m  gYw  [0m [1;36m[47m  gYw  [0m
   37m [37m  gYw   [37m[40m  gYw  [0m [37m[41m  gYw  [0m [37m[42m  gYw  [0m [37m[43m  gYw  [0m [37m[44m  gYw  [0m [37m[45m  gYw  [0m [37m[46m  gYw  [0m [37m[47m  gYw  [0m
 1;37m [1;37m  gYw   [1;37m[40m  gYw  [0m [1;37m[41m  gYw  [0m [1;37m[42m  gYw  [0m [1;37m[43m  gYw  [0m [1;37m[44m  gYw  [0m [1;37m[45m  gYw  [0m [1;37m[46m  gYw  [0m [1;37m[47m  gYw  [0m'

export PATH=$PATH:/home/jj/.spicetify
