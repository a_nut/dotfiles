#!/bin/zsh

# setting some stuff
autoload -Uz vcs_info
autoload add-zsh-hook

# vcs_info
zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:*' formats "%c%u %b"

-tqp-precmd() {
	vcs_info
	psvar[1]=$vcs_info_msg_0_
}
add-zsh-hook precmd -tqp-precmd

# PS2 with parser status on right
PROMPT2="%B%(!.%F{1}.%F{2})+%b%f "
RPROMPT2="%F{5}%B<%f%b%^"

# main prompt
if [[ -n $SSH_CONNECTION ]]; then
#	PROMPT="%F{0}%(!.%K{1}.%K{2}) %n%B@%m%b %K{4} %(1V.%v.%~%(1/./.)) %(?.%(1j.%K{0} %F{12}%j %F{7}.).%(1j.%K{0} %F{9}%? %F{12}%j %F{7}.%K{0} %F{9}%? %F{7}))%k%f
#%B%(!.%F{1}.%F{2})%#%b%f "
	PROMPT="%(!.%F{1}.%F{2})%n%B%f@%m%b %F{5}/ %F{4}%~%(1/./.)%(1V. %v.)%(1j. %F{5}/ %F{6}%j.)%(?.. %F{5}/ %F{1}%?)%k%f
%B%(!.%F{1}.%F{2})%#%b%f "
else
#	PROMPT="%F{0}%(!.%K{1}.%K{2}) %n %K{4} %(1V.%v.%~%(1/./.)) %(?.%(1j.%K{0} %F{12}%j %F{7}.).%(1j.%K{0} %F{9}%? %F{12}%j %F{7}.%K{0} %F{9}%? %F{7}))%k%f
#%B%(!.%F{1}.%F{2})%#%b%f "
	PROMPT="%(!.%F{1}.%F{2})%n %F{5}/ %F{4}%~%(1/./.)%(1V. %v.)%(1j. %F{5}/ %F{6}%j.)%(?.. %F{5}/ %F{1}%?)%k%f
%B%(!.%F{1}.%F{2})%#%b%f "
fi

# truncate the prompt when pressing the enter key
normal_prompt=$PROMPT
trunc_prompt='%B%(!.%F{1}.%F{2})%#%f%b '
trunc_prompt_accept_line() {
	PROMPT=$trunc_prompt
	zle reset-prompt
	zle accept-line
	PROMPT=$normal_prompt
}
zle -N trunc_prompt_accept_line
bindkey "^M" trunc_prompt_accept_line
