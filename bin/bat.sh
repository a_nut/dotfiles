#!/bin/sh

batf=$(acpi -b)

bat=$(echo "$batf" | awk '{print $4}' | sed 's/,//')
stat=$(echo "$batf" | awk '{print $3}' | sed 's/,//')

if [ ${#bat} -lt 4 ]; then
	bat="0$bat"
fi

case "$stat" in
	Charging)
		stat=
	;;
	Full)
		stat=
		;;
	*)
		stat=
	;;
esac

echo "$bat$stat"
