#!/bin/zsh
# curl 'https://gitlab.com/a_nut/dotfiles/-/raw/master/init.zsh' | zsh

which git || exit 1
which zsh || exit 1
which tmux|| echo press any key && read -k1 -s
which acpi|| echo press any key && read -k1 -s

[[ -z $1 ]] || [[ ! -f $1 ]] && target='.'
cd $target

git clone https://gitlab.com/a_nut/dotfiles .nutfiles
ln -s ./.nutfiles/.zsh
ln -s ./.nutfiles/.zshenv
ln -s ./.nutfiles/.vimrc
ln -s ./.nutfiles/.tmux.conf
git clone https://github.com/tarjoilija/zgen .zsh/zgen
